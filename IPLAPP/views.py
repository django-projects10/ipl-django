from django.http import HttpResponse
from .models import Matches, Deliveries
from django.shortcuts import render
from django.db.models import Count, Sum


def index(request):
    return HttpResponse('This is my first project in django')


def matches_per_season(request):
    result = (Matches.objects.values('season').annotate(
        no_of_matches=Count('id')).order_by('season'))
    return render(request, 'index.html', {'result': result})


def wins_per_season(request):
    result = (Matches.objects.values('season', 'winner').annotate(
        no_of_wins=Count('winner')).order_by('season', 'winner'))
    return render(request, 'wins.html', {'result': result})


def extra_runs_conceived(request):
    result = Deliveries.objects.filter(match__season=2016).values(
        'bowling_team').annotate(extras=Sum('extra_runs')).order_by('extras')
    return render(request, 'extras.html', {'result': result})


def top_10_economy(request):
    result = (Deliveries.objects.filter(match__season=2015).values('bowler')
              .annotate(
        economy=Sum('total_runs')*6/Count('id')).order_by('economy')[0:10])
    return render(request, 'economy.html', {'result': result})
