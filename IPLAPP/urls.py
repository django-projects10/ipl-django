from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('matches_per_season/', views.matches_per_season,
         name='matches_per_season'),
    path('wins_per_season/', views.wins_per_season, name='wins_per_season'),
    path('extra_runs_conceived/', views.extra_runs_conceived,
         name='extra_runs_conceived'),
    path('top_10_economy/', views.top_10_economy, name='top_10_economy')

]
