# Ipl Django

Ipl django

# Steps in the project

- Created virtual environment for the project.
- Activated virtual environment.
- Install requirements.
- Created IPL project.
- Created IPL App.
- Connected postgresql database in settings.py
- Done migrations.
- Create model classes and migrated.
- Create custom Django command and insterted Ipl Data set to tables.
- Writing ORM Queries in views.py and set url paths and getting respective results 
